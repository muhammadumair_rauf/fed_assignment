// For the document to be in ready state
$(document).ready(() => {

    // Setting Budget first time if not exist
    if (!localStorage.getItem('budget')) {
        localStorage.setItem('budget', 300);
    }

    // Setting the budget to show on HTML page
    var budget = +localStorage.getItem('budget');
    document.getElementsByClassName('popover__message')[0].innerHTML = (budget === 0) ? 'Target Acheived. Thanks to all donars' : '$' + budget + ' still needed for this project';

    // Setting End Date
    localStorage.setItem('endDate', '2019-04-20');

    // Count Remaining Days
    document.getElementById('day-left').innerHTML = countDayDiffrence(new Date(), new Date(localStorage.getItem('endDate')))

    // Setting donor count to show in HTML
    document.getElementsByClassName('donor-count')[0].innerHTML = localStorage.getItem('donorCount') ? localStorage.getItem('donorCount') : 0;

    var progress = ((300 - localStorage.getItem('budget')) / 300) * 100;
    // Setting the budget to show on HTML page
    document.getElementsByClassName('progress-bar')[0].setAttribute('style', 'width: ' + progress + '%');

    var donationElement = document.getElementById('donation');
    donationElement.addEventListener("input", () => {
        document.getElementsByClassName('btn-link')[0].innerHTML = 'Why Give $' + donationElement.value + '?';
    }, false);



});

// countDayDiffrence will calculate days between start and end dates
function countDayDiffrence(startDate, endDate) {

    let oneDay = 24 * 60 * 60 * 1000;
    return Math.round(Math.abs((startDate.getTime() - endDate.getTime()) / (oneDay))) + 1;

}