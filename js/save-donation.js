var form = document.getElementsByTagName('form')[0];

form.addEventListener("submit", (event) => {
    var donation = document.getElementById('donation').value;
    save(donation, event);

}, false);

// This function will save the donation
function save(donation, event) {
    var budget = +localStorage.getItem('budget')
    if (donation === '') {

        alert('Kindly fill out the amount field')
        event.preventDefault();

    } else if (+donation <= 0) {

        alert('Invalid Amount')
        event.preventDefault();

    } else if (+donation > budget && budget !== 0) {
        alert('Sorry!!! \nYou can not make donation greater than project budget');
        event.preventDefault();

    } else if (budget === 0) {

        alert('Thank you!!! \nNo need for donation now');
        event.preventDefault();


    } else {
        // Getting Budget from localStorage and then subtracting donation from budget
        var donation = document.getElementsByTagName('input')[0].value;
        localStorage.setItem('budget', budget - donation);

        if (!localStorage.getItem('donorCount')) {
            localStorage.setItem('donorCount', 1);
        } else {
            var donorCount = +localStorage.getItem('donorCount');
            donorCount++;
            localStorage.setItem('donorCount', donorCount);

        }
        alert("Thank You!!! \nDonation has been made");

    }

}